class Integer
  def simple?
    return false if self < 1
    return true if self == 1
    count = 0
    (1..self).each { |i| count += 1 if (self % i).zero? }
    count == 2
  end
end
