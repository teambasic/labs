require_relative "../6/miller_simplicity_checker.rb"

class Integer
  include MillerSimplicityChecker

  def simple_by_rabin_miller?(k = 10)
    return false if self < 2 || even?

    random = Random.new

    k.times do
      a = random.rand(2..self - 2)

      return false unless a.simplicity_witness_for? self
    end
    true
  end
end
