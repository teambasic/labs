require_relative "../1/euler_function"

class IntegerGaloisField
  attr_accessor :value
  attr_reader :modulus

  def initialize(value, modulus)
    @value = value.to_i
    @modulus = modulus.to_i
  end

  def ==(other)
    raise 'is not a IntegerGaloisField object' unless other.is_a?(IntegerGaloisField)
    value == other.value && modulus == other.modulus
  end

  def +(other)
    raise 'is not a IntegerGaloisField object' unless other.is_a?(IntegerGaloisField)
    IntegerGaloisField.new((value + other.value) % modulus, modulus)
  end

  def -(other)
    raise 'is not a IntegerGaloisField object' unless other.is_a?(IntegerGaloisField)
    IntegerGaloisField.new((value - other.residue) % modulus, modulus)
  end

  def -@
    IntegerGaloisField.new((-value) % modulus, modulus)
  end

  def *(other)
    raise 'is not a IntegerGaloisField object' unless other.is_a?(IntegerGaloisField)
    IntegerGaloisField.new((value * other.value) % modulus, modulus)
  end

  def recip
    IntegerGaloisField.new(
      integer_mod_recip(value, modulus), modulus
    )
  end

  def /(other)
    self * other.recip
  end

  def **(e)
    if value == 0
      if e == 0
        raise 'IntegerGaloisField:  0**0 undefined.'
      elsif e < 0
        puts e
        raise 'IntegerGaloisField **:  division by zero..'
      else
        return IntegerGaloisField.new(0, modulus)
      end
    end

    rv = IntegerGaloisField.new(1, modulus)
    xp = IntegerGaloisField.new(value, modulus)
    if e < 0
      xp = xp.recip
      e = -e
    end

    while (e != 0)
      if (e & 1) == 1
        rv *= xp;
      end
      e = e >> 1;
      xp *= xp;
    end
    return rv
  end

  def integer_mod_recip(x, m)
    if (x.gcd(m) != 1)
      puts "integer_mod_recip: impossible inverse #{x} mod #{m}"
      raise RuntimeError
    end
    phi = m.euler_function
    return integer_mod_exp(x, phi - 1, m)
  end

  def integer_mod_exp(x, e, m)
    if (e < 0)
      e = -e
      x = integer_mod_recip(x, m)
    end

    xp = x
    rv = 1

    while (e != 0)
      if (e & 1) == 1
        rv = (rv * xp) % m
      end
      e = e >> 1
      xp = (xp * xp) % m
    end
    return rv
  end

end
