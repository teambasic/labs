require_relative "probably_prime_counter"

prime_digits = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47].freeze
relations = []

t1 = Time.now
prime_digits.each_with_index do |p, i|
  prime_digits[(i + 1)..(prime_digits.length - 1)].each do |q|
    n = (p * q).to_f
    break if n > 100
    counter = ProbablyPrimeCounter.new(p, q)
    relations << counter.simplicity_witnesses_count.to_f / n
  end
end
t2 = Time.now

p "Probably Prime Counter: #{relations.sum.to_f / relations.length.to_f}. Time: #{t2 - t1}"
p "Rabin value: 0.25"
