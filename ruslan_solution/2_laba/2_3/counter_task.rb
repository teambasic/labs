require "java"
require "prime"
java_import "java.util.concurrent.Callable"
require_relative "probably_prime_counter"

class CounterTask
  include Callable

  def initialize(numbers, thread_number)
    @numbers = numbers
    @thread_number = thread_number
  end

  def call
    sum = 0
    count = 0
    @numbers.each do |semi_prime_digit|
      factorization = semi_prime_digit.prime_division
      p_d, q = factorization.length > 1 ? factorization.map(&:first) : factorization[0]
      puts "Thread: #{@thread_number} n = pq : #{semi_prime_digit} = #{p_d} * #{q}"
      counter = ProbablyPrimeCounter.new(p_d, q)
      sum += counter.simplicity_witnesses_count.to_f / semi_prime_digit.to_f
      count += 1
    end
    [sum, count]
  end
end
