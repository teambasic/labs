require_relative "counter_task"
require "prime"
require "java"
java_import "java.util.concurrent.FutureTask"
java_import "java.util.concurrent.LinkedBlockingQueue"
java_import "java.util.concurrent.ThreadPoolExecutor"
java_import "java.util.concurrent.TimeUnit"

class Integer
  def semi_prime?
    prime_division.map(&:last).inject(&:+) == 2
  end
end

semi_prime_numbers = (1..1_000_000).select(&:semi_prime?)
sums = 0
counts = 0

puts "Semi prime digits generated"

executor = ThreadPoolExecutor.new(4, 5, 60, TimeUnit::SECONDS, LinkedBlockingQueue.new)

tasks = []
index = 0

semi_prime_numbers.each_slice(semi_prime_numbers.length / 4) do |batch|
  probably_prime_counter_task = CounterTask.new(batch, index)
  index += 1
  task = FutureTask.new(probably_prime_counter_task)
  executor.execute(task)
  tasks << task
end

tasks.each do |t|
  sum, count = t.get
  sums += sum
  counts += count
end

puts sums.to_f / counts.to_f
