require_relative "../../1_laba/7/rabin_miller"
require_relative "../../1_laba/1/euler_function"

class ProbablyPrimeCounter
  def initialize(p, q)
    @p = p
    @q = q
  end

  def simplicity_witnesses_count
    s_set = d_subsets.map do |d_subset|
      d_subset.reduce(0) { |sum, digit| sum + digit.euler_function }
    end
    s_set.reduce(0) { |sum, s| sum + (s**2) }
  end

  private

  def d_subsets
    d_subsets = []
    uniq_bin_set = bin_set.uniq
    u = uniq_bin_set.max

    (u + 1).times do |i|
      d_subsets << united_d_set.select { |digit| bin(digit) == uniq_bin_set[i] }
    end

    d_subsets
  end

  def united_d_set
    @united_d_set ||= d_set_for(@p) & d_set_for(@q)
  end

  def bin_set
    @bin_set ||= united_d_set.map { |digit| bin(digit) }
  end

  def d_set_for(digit)
    digit -= 1
    (1..digit).select { |i| (digit % i).zero? }
  end

  def bin(n)
    power = 0
    power += 1 while (n % (2**power)).zero?
    power - 1
  end
end
