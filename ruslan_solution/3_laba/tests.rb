require_relative "witness_counter"
require 'test/unit'


class TestNaiveWitnessCounter < Test::Unit::TestCase
  def test_right_result
    examples = {
      [13, 41] => 6,
      [13, 7] => 18,
      [2, 2] => 1,
      [2, 3] => 1,
    }

    examples.each do |primes, answer|
      assert_equal(answer, NaiveWitnessCounter.new(*primes).result)
    end
  end
end


class TestWitnessCounter < Test::Unit::TestCase
  def test_right_result
    examples = {
      [7, 13, 31] => 270,
      [13, 29, 37] => 10,
      [11, 31, 41] => 750,
      [13, 41] => 6,
      [13, 7] => 18,
      [2, 3] => 1,
    }
    examples.each do |primes, answer|
      assert_equal(answer, WitnessCounter.new(*primes).result)
    end
  end
end
