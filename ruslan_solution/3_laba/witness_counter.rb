require_relative "../1_laba/1/euler_function"


class NaiveWitnessCounter
  def initialize(*primes)
    @n = primes.reduce(:*) # > 2
    @s = bin(@n - 1)
    @t = (@n - 1) >> @s
  end

  def result
    result = 1
    (2..@n).each do |a|
      result += 1 if check(a)
    end
    result
  end

  private
  def check(a)
    b = a ** @t
    return true if b % @n == 1

    (0..@s-1).each do |i|
      return true if b % @n == @n - 1
      b = b ** 2
    end
    false
  end

  def bin(n)
    power = 0
    power += 1 while (n % (2**power)).zero?
    power - 1
  end
end


class WitnessCounter
  def initialize(*primes)
    @primes = primes
    @n = primes.reduce(:*)
  end

  def result
    total = 1
    two_power = -1
    # NB от предложенного алгоритма ничего не осталось
    @primes.each do |prime|
      # Найдем множество D*
      gcd = (prime - 1).gcd(@n / prime - 1)
      if gcd == 1
        two_power = 0
        next
      end
      d = factorization(gcd)
      current = d[0][1]
      two_power = two_power == -1 ? current : [current, two_power].min
      # Сумма phi для множества делителей степени 0
      rock = d.lazy.drop(1).map {|num, power| num ** power}.reduce(:*) || 1
      total *= rock
    end
    total * coeff(@primes.length, two_power)
  end

  private
  def factorization(n)
    result = []
    (2..n).lazy.take_while{|i| i*i <= n}.each do |i|
      next if n % i != 0
      power = 0
      begin
        power += 1
        n /= i
      end while n % i == 0
      result.push([i, power]) 
    end
    result.push([n, 1]) if n > 1
    return result
  end

  def coeff(c, t)
    r = 2 ** c
    (r ** t - 1) / (r - 1) + 1
  end
end