require "sinatra"
require "sinatra/reloader"
require "slim"
require_relative "./1_laba/1/euler_function"
require_relative "./1_laba/5/brute_simplicity_checker"
require_relative "./1_laba/6/miller_simplicity_checker"
require_relative "./1_laba/7/rabin_miller"

get "/" do
  slim :index
end

get "/laba1/euler" do
  slim :laba1_euler
end

post "/laba1/euler" do
  @euler_answer = value ? value.to_i.euler_function : input_error
  slim :laba1_euler 
end

get "/laba1/brute_simpliness" do
  slim :laba1_brute_simpliness
end

post "/laba1/brute_simpliness" do
  @is_simple = value ? value.to_i.simple? : input_error
  slim :laba1_brute_simpliness
end

get "/laba1/miller_simpliness" do
  slim :laba1_miller_simpliness
end

post "/laba1/miller_simpliness" do
  @is_simple = a && n ? a.to_i.simplicity_witness_for?(n.to_i) : input_error
  slim :laba1_miller_simpliness
end

get "/laba1/miller_rabin" do
  slim :laba1_miller_rabin
end

post "/laba1/miller_rabin" do
  @is_simple = n && k ? n.to_i.simple_by_rabin_miller?(k.to_i) : input_error
  slim :laba1_miller_rabin
end

%i[value a n k].each do |method_name|
  define_method(method_name) { params.fetch(method_name.to_s, false) }
end

def input_error
  "Error input value - must be digit"
end

