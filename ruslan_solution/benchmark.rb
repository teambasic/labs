require "benchmark"
require_relative "./3_laba/witness_counter"
require_relative "./2_laba/probably_prime_counter"


def count_from_file(counter)
  File.open(ARGV[0], "r") do |file|
    file.each_line.lazy.map do |line|
      p, q = line.split(" ").map(&:to_i)
      counter.new(p, q).result if p != q
    end.first(5000)
  end
end


Benchmark.bm(20) do |x|
  x.report("WitnessCounter:")   { count_from_file(WitnessCounter) }
  x.report("ProbablyPrimeCounter:")   { count_from_file(ProbablyPrimeCounter) }
end
