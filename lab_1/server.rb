require 'sinatra'
require 'pry'
require './brute_force_simpliness'
require './euler_function'
require './miller_rabin'
require './simplicity_checker'

get '/' do
  File.read('index.html')
end

get '/lab_1/brute_force_simpliness' do
  if input_valid?(params)
    { simple: params['n'].to_i.simple? }.to_json
  else
    { error: 'input invalid' }
  end
end

get '/lab_1/euler_function' do
  if input_valid?(params)
    { value: params['n'].to_i.euler_function }.to_json
  else
    { error: 'input invalid' }
  end
end

def input_valid?(params)
  params['n'].to_i > 0
end
