require_relative "simplicity_checker.rb"

class Integer
  include SimplicityChecker

  def simple_by_rabin_miller?(k = 2)
    return false if self < 2 || even?

    random = Random.new

    k.times do
      a = random.rand(2..self - 1)

      return false unless a.simplicity_witness_for? self
    end
    true
  end
end
