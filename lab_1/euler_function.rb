class Integer
  def euler_function
    coprime_digits = (1..self).select { |i| gcd(i).abs == 1 }
    coprime_digits.length
  end

  def gcd(a)
    b = self
    Integer.gcd(a, b)
  end

  def self.gcd(a, b)
    while b != 0
      t = b
      b = a % b
      a = t
    end
    a
  end
end
