module SimplicityChecker
  def simplicity_witness_for?(n)
    t = n - 1
    s = 0

    while t.even?
      t /= 2
      s += 1
    end

    x = (self**t) % n

    return true if [1, n - 1].include? x

    (s - 1).times do
      x = (x**2) % n
      return false if x == 1
      return true if x == n - 1
    end
    return false if x != n - 1
  end
end

class Integer
  include SimplicityChecker
end
