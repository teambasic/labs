class Integer
  def simple?
    return false if self <= 1
    return true if self == 2
    (2..self ** 0.5).all? { |i| not (self % i).zero? }
  end
end

